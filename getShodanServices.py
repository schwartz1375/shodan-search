#!/usr/bin/env python

__author__    = 'Matthew Schwartz (@schwartz1375)'
__email__     = 'matthew.r.schwartz[at]gmail.com'
__version__   = '0.1'

import requests, os
import simplejson as json

def run(apiKey):
	r = requests.get('https://api.shodan.io/shodan/services?key='+apiKey, verify=True)
	#print("Accessing: " + r.url)
	#to do a REST (http post)
	print json.dumps(r.json(), indent=4)

def getAPIkey(key_name, key_text='API Key'):
    	keyfile = 'api.keys'
    	if os.path.exists(keyfile):
        	for line in open(keyfile):
            		key, value = line.split('::')[0], line.split('::')[1]
            		if key == key_name:
                		return value.strip()
	try: key = raw_input("Enter " +str(key_name)+ " %s: " % (key_text))
	except KeyboardInterrupt: return ''
	if key:
        	file = open(keyfile, 'a')
        	file.write('%s::%s\n' % (key_name, key))
        	file.close()
    	return key

if __name__ == '__main__':
	apiKey = getAPIkey('shodan')
	print "Calling Shodan to get the list of all services that Shodan crawls...\n"
	run(apiKey)
