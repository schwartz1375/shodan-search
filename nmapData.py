#!/usr/bin/env python

__author__    = 'Matthew Schwartz (@schwartz1375)'
__email__     = 'matthew.r.schwartz[at]gmail.com'
__version__   = '1.0'

'''
NOTE:
-When using the -Pn the system will report the state as "up" because it didn't tests it via ping (this normal mmap behavior
-Without root access we can not do privileged scans i.e. -sS p
-nmapPortRange=None - By default, Nmap scans the most common 1,000 ports for each protocol.
'''

import sqlite3, argparse, sys
import nmap #easy_install python-nmap

dbFileName = "hosts.db"
scanType = '-Pn -sT' 
nmapPortRange = None

def init_db(dbFileName):
    print "Setting up the DB for the first time"
    conn = sqlite3.connect(dbFileName)
    c = conn.cursor()
    c.execute('create table if not exists listeners (IP text, foundPorts text, nmapCMDexec text, scanType text, portRange text, nmapVer text, timeStamp timestamp)')
    conn.commit()
    conn.close()
    
    
def queryDB(dbFileName, searchTerm):
    print "Query db for possible targets"
    conn = None
    cursor = None
    queryStr="SELECT IP FROM shodan WHERE searchterm='"+searchTerm+"'"
    targetIPs = []
    try:
        conn = sqlite3.connect(dbFileName)
        cursor = conn.cursor()
        cursor.execute(queryStr)
        for row in cursor:
            #print "IP = " + row[0] + " Port: "+ row[1]
            #print "IP = " + row[0]
            targetIPs.append(row[0])
        #print targetIPs
        return targetIPs
    except sqlite3.Error, e:
        print "Error %s:" % e.args[0]
        sys.exit(1)
    finally:
        if conn:
            conn.close()  
    
def scanTarget(dbFileName, targetIPs):             
    print "Scan IP for open ports"
    myIPs = ""
    strgcom= ""
    for ipAddr in targetIPs:
        myIPs = myIPs+strgcom+ipAddr
        strgcom= ", " 
    print "Target IP(s): "+ myIPs #str(targetIPs)
    for IPaddr in targetIPs:
        IPaddr, openPorts, nmapCMD, scanType, portRange, nmapVer = nmapTarget(str(IPaddr))
        #print IPaddr, openPorts
        updateDB(dbFileName, IPaddr, openPorts, nmapCMD, scanType, portRange, nmapVer)
        
def nmapTarget(IPaddr):
    # TODO: missing error checking on the namp scan
    nmapCMD=""
    myPortRange=""
    nmapVer=""
    try:
        nm = nmap.PortScanner()
        nmapVer = str(nm.nmap_version())
    except nmap.PortScannerError:
        print "AW SNAP!" 
        print('Nmap not found', sys.exc_info()[0])
        sys.exit(0)
    except:
        print "AW SNAP!" 
        print("Unexpected error:", sys.exc_info()[0])
        sys.exit(0)
    # TODO -check if we are root, without root access we cant do -sS scan i.e. if (os.getuid() == 0):
    try:
        nm.scan(hosts=IPaddr, ports=nmapPortRange, arguments=scanType)  
        nmapCMD = nm.command_line()    
        print nmapCMD
        openPorts=[]                   
        for host in nm.all_hosts():
            print('-'*40)
            print('Host : {0} ({1})'.format(host, nm[host].hostname()))
            print('State : {0}'.format(nm[host].state()))
            print nm.csv()
            print('-'*40)
            for port in nm[host].all_tcp():
                #print nmScan.command_line()
                state=nm[host]['tcp'][int(port)]['state']
                #print "[*] " + host + " tcp/"+str(port) +" "+state
                if state == 'open':
                    print "Port %d is open!" % port
                    openPorts.append(port)
        #print openPorts
        if nmapPortRange == None:
            portRange = "nmap default (see man page for details)"
        else:
            portRange = nmapPortRange
        return IPaddr, openPorts, nmapCMD, scanType, portRange, nmapVer
    except Exception, e:
        print "AW SNAP!" 
        print("Unexpected error:", str(e))
        sys.exit(0)

        
def updateDB(dbFileName, IPaddr, openPorts, nmapCMD, scanType, portRange, nmapVer):
    #DB col: IPaddr, Ports, date
    conn = None
    cursor = None
    myPorts = ""
    strgcom= ""
    for port in openPorts:
        myPorts = myPorts+strgcom+str(port)
        strgcom= ", " 
    mystrg="INSERT INTO listeners (IP,foundPorts,nmapCMDexec, scanType,portRange,nmapVer,timeStamp) VALUES \
            ('"+IPaddr+"','"+myPorts+"','"+nmapCMD+"','"+scanType+"','"+portRange+"','"+nmapVer+"',CURRENT_TIMESTAMP);"
    #print mystrg
    try:
        conn = sqlite3.connect(dbFileName)
        cursor = conn.cursor()
        cursor.execute(mystrg)
        conn.commit()
    except sqlite3.Error, e:
        print "Error %s:" % e.args[0]
        sys.exit(1)
    finally:
        if conn:
            conn.close()  
    

def displayPossibleTerms(dbFileName):
    print "Query db for possible terms"
    conn = None
    cursor = None
    queryStr="SELECT DISTINCT searchterm FROM shodan"
    try:
        conn = sqlite3.connect(dbFileName)
        cursor = conn.cursor()
        cursor.execute(queryStr)
        for row in cursor:
            print "Searchterms = " + row[0]
    except sqlite3.Error, e:
        print "Error %s:" % e.args[0]
        sys.exit(1)
    finally:
        if conn:
            conn.close()  
    
def main(dbFileName, searchTerm):
    init_db(dbFileName)
    targetIPs = queryDB(dbFileName, searchTerm)
    if len(targetIPs) != 0:
        scanTarget(dbFileName, targetIPs)
            
if __name__ == '__main__':
    parser = argparse.ArgumentParser(prog='checkData')
    parser.add_argument("-s", "--searchterm", help="Shodan search term to nmap ")
    parser.add_argument("-d", "--displayterms", help="display possible terms", action='count')
    args = parser.parse_args()

    searchTerm = args.searchterm
    displayTerms = args.displayterms
    if ( searchTerm == None and displayTerms == None):
        print parser.print_help()
        exit(0)
    
    if (displayTerms == 1 ):
        displayPossibleTerms(dbFileName)
        
    
    if (searchTerm != None ):
        #print searchTerm
        main(dbFileName, searchTerm)
        
        

    