#!/usr/bin/env python

__author__    = 'Matthew Schwartz (@schwartz1375)'
__email__     = 'matthew.r.schwartz[at]gmail.com'
__version__   = '1.1'

import sqlite3, webbrowser, os


headerHTML1 = """\
<html>
<head>
<title>Shodan Data</title>
<body onload="initialize()">
    <style type="text/css">
      html { height: 100% }
      body { height: 100%; margin: 0; padding: 0 }
      #map_canvas { height: 100% }
    </style>
<h2 align="center">Shodan Data for the search term: """


headerHTML2 = """ <em><font color="#3333FF" size="-1"></font></em></h2> 

<script type="text/javascript" src="http://maps.google.com/maps/api/js?sensor=false"></script>
<script type="text/javascript">

"""


boadyHTML = """\

var bounds = new google.maps.LatLngBounds();
var map;
var infowindowLevel = 0;

function initialize() {
  var myOptions = {
    //zoom: 15,
    navigationControl: true,
    mapTypeControl: true,
    scaleControl: true,
    center: new google.maps.LatLng(0, 0),
    mapTypeId: google.maps.MapTypeId.HYBRID
  }
  
  map = new google.maps.Map(document.getElementById("map_canvas"), myOptions);
  
  if (detectors.length > 0) {
  	setMarkers(map, detectors);
  }
}

function setMarkers(map, locations) {
  for (var i = 0; i < locations.length; i++) {
    var detector = locations[i];
    var myLatLng = new google.maps.LatLng(detector[1], detector[2]);
    var image = "pushpin.png";
    var marker = new google.maps.Marker({
        position: myLatLng,
        map: map,
        icon: image,
        title: detector[0]
    });
    attachInfowindow(marker, i, detector);
    bounds.extend(myLatLng);
    map.fitBounds(bounds);
  }
}
function attachInfowindow(marker, number, det) {
  var infowindow = new google.maps.InfoWindow(
      { content: "IP: " + det[0] + " Port: "+det[3]+" Lat: " + det[1] + " Lon: " + det[2]
      });
  google.maps.event.addListener(marker, 'click', function() {
    infowindow.setZIndex(++infowindowLevel);
    infowindow.open(map,marker);
  });
}
</script>	
		<div id="map_canvas" style="width:100%; height:100%"></div>
  </body>

</html>
"""    
def getData(dbName, searchTerm):
    conn = sqlite3.connect(dbName)
    c = conn.cursor()
    myvar = ""
    strgcom = ""
    cmd="SELECT * FROM shodan WHERE Latitude IS NOT NULL and Longitude IS NOT NULL and searchterm='"+searchTerm+"'"
    for row in c.execute(cmd):
    	#IP, lat, lon, port
    	#print row[2], row[3], row[4], row[8]
    	myvar = myvar + strgcom+ "['"+row[2]+"','"+row[3]+"','"+row[4]+"','"+row[8]+"']"
    	strgcom= ","   
    conn.close()
    return myvar

def plotData(dbName, searchTerm):
	ret = getData(dbName, searchTerm)
	queryData = "var detectors = [" + ret +"];" 
	htmlPage = headerHTML1 + searchTerm+ headerHTML2 + queryData + boadyHTML
	#print htmlPage
	with open("myShodanData.html", "w") as text_file:
		text_file.write(htmlPage)
    	path = os.getcwd()
    	w = webbrowser.get()
    	w.open('file://%s/myShodanData.html' % path)

def getAllData(dbName):
    	conn = sqlite3.connect(dbName)
    	c = conn.cursor()
    	myvar = ""
    	strgcom = ""
    	cmd="SELECT * FROM shodan WHERE Latitude IS NOT NULL and Longitude IS NOT NULL"
    	for row in c.execute(cmd):
      		#IP, lat, lon, port
      		#print row[2], row[3], row[4], row[8]
      		myvar = myvar + strgcom+ "['"+row[2]+"','"+row[3]+"','"+row[4]+"','"+row[8]+"']"
      		strgcom= ","   
    	conn.close()
    	return myvar

if __name__ == "__main__":
	searchTerm = "all entries in the DB"
  	ret = getAllData("hosts.db")
  	queryData = "var detectors = [" + ret +"];" 
	#queryData = "var detectors = [" + getData("hosts.db") +"];" 
	htmlPage = headerHTML1 + searchTerm + headerHTML2 + queryData + boadyHTML
	print htmlPage
