#!/usr/bin/env python

__author__    = 'Matthew Schwartz (@schwartz1375)'
__email__     = 'matthew.r.schwartz[at]gmail.com'
__version__   = '1.0'
'''
FOSS geoIP location URL services
1. www.hostip.info
2. http://ipinfodb.com
'''

import urllib
import json

class IPInfo_hostip():
    def __init__(self):
        self.name = 'IPInfo'
        
    def getIPInfo(self, ipAddr):
        response = urllib.urlopen('http://api.hostip.info/get_html.php?ip='+ipAddr+'&position=true').read()
        return response
    
    def getIPInfoJSON(self, ipAddr):
        response = urllib.urlopen('http://api.hostip.info/get_json.php?ip='+ipAddr+'&position=true').read()
        return response

    def getIPInfoXML(self, ipAddr):
        response = urllib.urlopen('http://api.hostip.info/?ip?ip='+ipAddr+'&position=true').read()
        return response 
    
    def getJSONLatLon(self, data):
        myLat = None
        myLong = None        
        #mydecoder = json.JSONDecoder()
        #for key, val in mydecoder.decode(data).items(): #the following is a short hand versions
        for key, value in json.JSONDecoder().decode(data).items():
            #print key, val #print hash map of JSON data
            if key == 'lat':
                myLat = value
            if key == 'lng':
                myLong = value
        return myLat, myLong

class IPInfo_ipinfodb():
    def __init__(self, apikey):
        self.apikey = apikey
    
    def getIPInfo(self, ipAddr):
        response = urllib.urlopen('http://api.ipinfodb.com/v3/ip-city/?key='+self.apikey+'&ip='+ipAddr+'&format=json').read()
        return response
    
    def getJSONLatLon(self, data):
        myLat = None
        myLong = None
        mycountryCode = None
        mycountryName = None
        mycityName = None
        for key, value in json.JSONDecoder().decode(data).items():
            #print key, val #print hash map of JSON data
            if key == 'latitude':
                myLat = value
            if key == 'longitude':
                myLong = value
            if key == 'countryCode':
                mycountryCode = value
            if key == 'countryName':
                mycountryName = value
            if key == 'cityName':
                mycityName = value
        return myLat, myLong, mycountryCode, mycountryName, mycityName 
    
if __name__ == "__main__":
    #Test funcations
    ipAddr = "8.8.8.8"
    '''
    Using Data from www.hostip.info
    '''
    ipinfo = IPInfo_hostip()
    data = ipinfo.getIPInfo(ipAddr)
    print data + '\n'
    data = ipinfo.getIPInfoXML(ipAddr)
    print data +'\n'
    data = ipinfo.getIPInfoJSON(ipAddr)
    print data + '\n'            
    #JSON parse    
    lat, lng = ipinfo.getJSONLatLon(data)
    print lat, lng
    
    '''
    Using Data from http://ipinfodb.com
    '''
    apikey = "INSERT YOUR API KEY HERE"
    ipinfo = IPInfo_ipinfodb(apikey)
    data = ipinfo.getIPInfo(ipAddr)
    print data + '\n'
    myLat, myLong, mycountryCode, mycountryName, mycityName = ipinfo.getJSONLatLon(data)
    print myLat, myLong, mycountryCode, mycountryName, mycityName