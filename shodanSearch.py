#!/usr/bin/env python

__author__    = 'Matthew Schwartz (@schwartz1375)'
__email__     = 'matthew.r.schwartz[at]gmail.com'
__version__   = '1.5.0'


import sqlite3, re, socket, sys, argparse, os
from shodan import WebAPI
from mapShodanData import plotData
from geolocation import IPInfo_ipinfodb

dbName = "hosts.db"
#regex = re.compile(r"^\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}$")
regex = re.compile(r"""
    ^
    (?:
      # Dotted variants:
      (?:
        # Decimal 1-255 (no leading 0's)
        [3-9]\d?|2(?:5[0-5]|[0-4]?\d)?|1\d{0,2}
      |
        0x0*[0-9a-f]{1,2}  # Hexadecimal 0x0 - 0xFF (possible leading 0's)
      |
        0+[1-3]?[0-7]{0,2} # Octal 0 - 0377 (possible leading 0's)
      )
      (?:                  # Repeat 0-3 times, separated by a dot
        \.
        (?:
          [3-9]\d?|2(?:5[0-5]|[0-4]?\d)?|1\d{0,2}
        |
          0x0*[0-9a-f]{1,2}
        |
          0+[1-3]?[0-7]{0,2}
        )
      ){0,3}
    |
      0x0*[0-9a-f]{1,8}    # Hexadecimal notation, 0x0 - 0xffffffff
    |
      0+[0-3]?[0-7]{0,10}  # Octal notation, 0 - 037777777777
    |
      # Decimal notation, 1-4294967295:
      429496729[0-5]|42949672[0-8]\d|4294967[01]\d\d|429496[0-6]\d{3}|
      42949[0-5]\d{4}|4294[0-8]\d{5}|429[0-3]\d{6}|42[0-8]\d{7}|
      4[01]\d{8}|[1-3]\d{0,9}|[4-9]\d{0,8}
    )
    $
""", re.VERBOSE | re.IGNORECASE)

def hostname_resolves(hostname):
	try:
		socket.gethostbyname
		return True
	except socket.error:
		return False

def get_key(key_name, key_text='API Key'):
    keyfile = 'api.keys'
    if os.path.exists(keyfile):
        for line in open(keyfile):
            key, value = line.split('::')[0], line.split('::')[1]
            if key == key_name:
                return value.strip()
    try: key = raw_input("Enter " +str(key_name)+ " %s: " % (key_text))
    except KeyboardInterrupt: return ''
    if key:
        file = open(keyfile, 'a')
        file.write('%s::%s\n' % (key_name, key))
        file.close()
    return key

# Create a connection to the Shodan API
api = WebAPI(get_key('shodan'))
def init_db(fileName):
    conn = sqlite3.connect(fileName)
    c = conn.cursor()
    c.execute('create table if not exists shodan (City text, Updated text, IP text, Latitude text, Longitude text, country_name text, country_code text, Data text, Port text, OS text, Hostnames text, searchterm text)')
    conn.commit()
    conn.close()

#Pull addition or missing GEO location information
def getIPInfo(ipAddr):
    ipinfo = IPInfo_ipinfodb(get_key('ipinfodb'))
    data = ipinfo.getIPInfo(ipAddr)
    myLat, myLong, mycountryCode, mycountryName, mycityName = ipinfo.getJSONLatLon(data)
    return myLat, myLong, mycountryCode, mycountryName, mycityName
    
def updateDB(dbName, mystrg, ipAddr, port):
	conn = None
	cursor = None
	cleanstr="DELETE FROM shodan WHERE IP='"+ipAddr+"' and Port='"+port+"'"
	try:
		conn = sqlite3.connect(dbName)
		cursor = conn.cursor()
		cursor.execute(cleanstr)
		conn.commit()
		cursor.execute(mystrg)
		conn.commit()
	except sqlite3.Error, e:
		print "Error %s:" % e.args[0]
		exit(1)
	finally:
		if conn:
			conn.close()  

#Queries Shodan for a search term and then stores results in a list of dictionaries 
def query_Shodan(searchterm, callback):
	templist = []
	try:
		#while True:
		#Search Shodan
		results = api.search(searchterm)
		# Show the results
		print '************************************************************'   
		print 'Results found: %s' % results['total']
		print 'Using the following search critica: %s' % searchterm
		print '************************************************************'   
		print ''
		#Construct a temp dictionary to store results
		for result in results['matches']:
			#add searchterm so the DB gets it
			result['searchterm']=searchTerm
			value = result.values()
			key = result.keys()
			#print result.keys() #list all keys in the Dictionaries (unsorted)
			#sorted(result.items(), key=lambda (key, value): (value,key))
			#for i in range (len(result)):
				#print '%s: %s' % (key[i], value[i])
			temp = {}
			temp["searchterm"] = searchterm
			ipAddr = '%s' %result['ip'] 
			temp["IP"] = ipAddr.encode('ascii', 'replace')
			if result.has_key('hostnames') is True:
				hostnames = ''.join(result['hostnames'])
				temp["Hostnames"] = hostnames
			port = '%s' %result['port']
			temp["Port"] = port.encode('ascii', 'replace')
			data = '%s' %result['data']	
			data = data.encode('ascii', 'replace')
			#To prevent DB insert issues we remove " and '
			data =  data.replace('"', '')
			data = data.replace("'", '')
			temp["Data"] = data
			updated = '%s' %result['updated']
			temp["Updated"] = updated.encode('ascii', 'replace')
			#Get GEO Location incase shodan doesn't provide it
			myLat, myLong, mycountryCode, mycountryName, mycityName = getIPInfo(ipAddr)
			if result.has_key('city') is True | result.has_key('city') == 'None':
				city = '%s' %result['city']
				#To prevent DB insert issues we remove " and '
                		city = city.replace('"', '')
                		city = city.replace("'", '')			
				temp["City"] = city.encode('ascii', 'replace')
			else:
				temp["City"] = mycityName
			if result.has_key('country_code') is True | result.has_key('city') == 'None':
				ccode = '%s' %result['country_code']
				temp["country_code"] = ccode.encode('ascii', 'replace')
			else:
				temp["country_code"] = mycountryCode
			if result.has_key('country_name') is True | result.has_key('country_name') == 'None':
				cname = '%s' %result['country_name']
				temp["country_name"] = cname.encode('ascii', 'replace')
			else:
				temp["country_name"] = mycountryName
			if result.has_key('latitude') is True | result.has_key('latitude') == 'None':
				latitude = '%s' %result['latitude']
				temp["Latitude"] = latitude.encode('ascii', 'replace')
			else:
				temp["Latitude"] = myLat
			if result.has_key('longitude') is True | result.has_key('longitude') == 'None':
				longitude = '%s' %result['longitude']
				temp["Longitude"] = longitude.encode('ascii', 'replace')
			else:
				temp["Longitude"] = myLong
			if result.has_key('os') is True:
				os = '%s' %result['os']		
				temp["OS"] = os.encode('ascii', 'replace')	
			templist.append(temp)
			#Dump all the data to the screen
			#callback(temp)
	except Exception, e:
		#No results found, print no 'matches'
		print 'No %s\r' %e
		exit()
	#Returns a list of dictionary objects. Each dictionary is a result
	return templist

def print_result(info):
	print info

def displayPossibleTerms(dbName):
    print "Query db for possible terms"
    conn = None
    cursor = None
    queryStr="SELECT DISTINCT searchterm FROM shodan"
    try:
        conn = sqlite3.connect(dbName)
        cursor = conn.cursor()
        cursor.execute(queryStr)
        for row in cursor:
            print "Searchterms = " + row[0]
    except sqlite3.Error, e:
        print "AW SNAP!  %s:" % e.args[0]
        exit(1)
    finally:
        if conn:
            conn.close()  
	exit(0)        

def lookupHost(dbName, target):
	try:
	        ip = socket.gethostbyname(target)
        	print "*"*50
        	print "Looking up %s on shodanhq" % ip
		host = api.host(ip)
		results = 0
		for item  in host['data']:
			results = results + 1
		print "Results found: %s" % results 
		print "*"*50
		#print general information
		print """
			IP: %s
			Country: %s
			City: %s
		"""  % (host['ip'], host.get('country', None), host.get('city', None)) 
		#print all the banners
		for item in host['data']:
			print """
				Port: %s
				Banner: %s
			"""% (item['port'], item['banner'])
	except Exception, e:
		print 'AW SNAP! %s' % e
	finally:
		exit(0)

def execSearch(searchTerm):
	#Create DB if it doesn't exist
	init_db(dbName)
	
	#Usage with inbuilt printing
	list = query_Shodan(searchTerm, print_result)

	#Load Data into the SQLite DB
	for result in list:
		mystr1 = "INSERT INTO shodan ("	
		mystr2 = " values ( "
		strgcom = ""
		ipAddr = None
		port = None
		value = result.values()
		key = result.keys()
		#print result.keys() #list all keys in the Dictionaries (unsorted)
		sorted(result.items(), key=lambda (key, value): (value,key))
		for i in range (len(result)):
			print '%s: %s' % (key[i], value[i])
			if result.has_key('IP') is True:
				ipAddr = result['IP'] 
			if result.has_key('Port') is True:
				port = result['Port']
			mystr1 =mystr1+strgcom+key[i]
			mystr2 = mystr2+strgcom+"'"+ value[i] +"'"
			strgcom= ","   
		mystr = mystr1 + ") "+mystr2 +")"
		updateDB(dbName, mystr, ipAddr, port)
		print '**********************************************'  
	#plotData(dbName, searchTerm)  
	return dbName, searchTerm

if __name__ == "__main__":
	parser = argparse.ArgumentParser("Python Shodan query tool with mapping")
	parser.add_argument("-s", "--searchterm", help="Shodan Search term", type=str)
	parser.add_argument("-l", "--targetlist", help="Shodan Search using a list of IP addresses or FQNs comma seperated", type=str)#./shodanSearch.py -l "216.219.143.14,216.219.143.0/24" 
	parser.add_argument("-f", "--targetfile", help="Reads in a file of IP addresses or FQNs", type=str)	
	parser.add_argument("-t", "--target", help="Get all available information on an IP", type=str)
	parser.add_argument("-p", "--plotsearch", help="Display a map of prior search via its searchterm in the DB", type=str)
    	parser.add_argument("-d", "--displayterms", help="display possible terms", action='count')

	args = parser.parse_args()
	searchTerm = args.searchterm
	targetIPlist = args.targetlist
	targetFile = args.targetfile
	plotSearch = args.plotsearch
    	displayTerms = args.displayterms
	target = args.target

	if targetIPlist:
		#print "String is:" + ipList
		targetList=targetIPlist.split(",")
		#print "The target is: "+ str(targetList)
		for ipAddr in targetList:
			result = regex.match(ipAddr) is not None
			if result:
				print "Input is a vaild IP address!"
				ipAddr = socket.gethostbyname(ipAddr.strip())
				execSearch("net:"+ipAddr.strip())
			else:
				print "Input is not a valid IP address, checking via hostname"
				if hostname_resolves(ipAddr):
					ipAddr = socket.gethostbyname(ipAddr.strip())
					execSearch("net:"+ipAddr.strip())
				else:
					print "Failed to resolve host name!"
		exit(0)
	if targetFile:
        	f = open(targetFile, 'r')
		lines = f.readlines()
		for ipAddr in lines:	
			result = regex.match(ipAddr) is not None
			if result:
				print "Input is a vaild IP address!"
				ipAddr = socket.gethostbyname(ipAddr.strip())
				execSearch("net:"+ipAddr.strip())
			else:
				print "Input is not a valid IP address, checking via hostname"
				if hostname_resolves(ipAddr):
					ipAddr = socket.gethostbyname(ipAddr.strip())
					execSearch("net:"+ipAddr.strip())
				else:
					print "Failed to resolve host name!"
		exit(0)	
	if plotSearch:
		plotData(dbName, plotSearch)
		print "Displaying data via the default web web browser"
		exit(0)
	if target:
		lookupHost(dbName, target)
    	if (displayTerms == 1):
		displayPossibleTerms(dbName)
	if (searchTerm != None):
		dbName, searchTerm=execSearch(searchTerm)
		plotData(dbName, searchTerm)
		exit(0)
	else:
		print parser.print_help()
